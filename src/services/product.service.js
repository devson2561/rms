// import axios from "axios";

const mock = [
  {
    image: "https://f.ptcdn.info/032/060/000/pfwzp178ojAaWr5mXyB-o.jpg",
    name: "กระเพราไก่",
    price: 20,
    category: "thai",
    group: 'fried'
  },
  {
    image: "https://f.ptcdn.info/032/060/000/pfwzp178ojAaWr5mXyB-o.jpg",
    name: "กระเพราเป็ด",
    price: 20,
    category: "thai",
    group: 'fried'
  },
  {
    image: "https://f.ptcdn.info/032/060/000/pfwzp178ojAaWr5mXyB-o.jpg",
    name: "กระเพราหมูสับ",
    price: 20,
    category: "thai",
    group: 'fried'
  },
  {
    image: "https://www.spokedark.tv/wp-content/uploads/2017/10/115-7.jpg",
    name: "หมูหัน",
    price: 2000,
    category: "chinese",
    group: 'grill'
  },
  {
    image: "https://f.ptcdn.info/032/060/000/pfwzp178ojAaWr5mXyB-o.jpg",
    name: "กระเพราไก่",
    price: 20,
    category: "thai",
    group: 'fried'
  },
  {
    image: "https://f.ptcdn.info/032/060/000/pfwzp178ojAaWr5mXyB-o.jpg",
    name: "กระเพราเป็ด",
    price: 20,
    category: "thai",
    group: 'fried'
  },
  {
    image: "https://f.ptcdn.info/032/060/000/pfwzp178ojAaWr5mXyB-o.jpg",
    name: "กระเพราหมูสับ",
    price: 20,
    category: "thai",
    group: 'fried'
  },
  {
    image: "https://www.spokedark.tv/wp-content/uploads/2017/10/115-7.jpg",
    name: "หมูหัน",
    price: 2000,
    category: "chinese",
    group: 'grill'
  },
  {
    image: "https://www.spokedark.tv/wp-content/uploads/2017/10/115-7.jpg",
    name: "เป็ดหัน",
    price: 2000,
    category: "chinese",
    group: 'grill'
  },
  {
    image: "https://www.spokedark.tv/wp-content/uploads/2017/10/115-7.jpg",
    name: "แมวหัน",
    price: 2000,
    category: "chinese",
    group: 'grill'
  },
  {
    image: "https://www.spokedark.tv/wp-content/uploads/2017/10/115-7.jpg",
    name: "เป็ดหัน",
    price: 2000,
    category: "chinese",
    group: 'grill'
  },
  {
    image: "https://www.spokedark.tv/wp-content/uploads/2017/10/115-7.jpg",
    name: "แมวหัน",
    price: 2000,
    category: "chinese",
    group: 'grill'
  },
  {
    image: "https://www.spokedark.tv/wp-content/uploads/2017/10/115-7.jpg",
    name: "เป็ดหัน",
    price: 2000,
    category: "chinese",
    group: 'grill'
  },
  {
    image: "https://f.ptcdn.info/855/044/000/ob7t9j3ebl9adHWNppO-o.jpg",
    name: "สปาเกตตี้",
    price: 500,
    category: "inter",
    group: 'fried'
  },
  {
    image: "https://f.ptcdn.info/855/044/000/ob7t9j3ebl9adHWNppO-o.jpg",
    name: "สปาเกตตี้",
    price: 500,
    category: "inter",
    group: 'fried'
  },
  {
    image: "https://f.ptcdn.info/855/044/000/ob7t9j3ebl9adHWNppO-o.jpg",
    name: "สปาเกตตี้",
    price: 500,
    category: "inter",
    group: 'fried'
  },
  {
    image: "https://f.ptcdn.info/855/044/000/ob7t9j3ebl9adHWNppO-o.jpg",
    name: "สปาเกตตี้",
    price: 500,
    category: "inter",
    group: 'fried'
  },
  {
    image: "https://f.ptcdn.info/855/044/000/ob7t9j3ebl9adHWNppO-o.jpg",
    name: "สปาเกตตี้",
    price: 500,
    category: "inter",
    group: 'fried'
  },
  {
    image: "https://f.ptcdn.info/855/044/000/ob7t9j3ebl9adHWNppO-o.jpg",
    name: "สปาเกตตี้",
    price: 500,
    category: "inter",
    group: 'fried'
  },
  {
    image: "https://f.ptcdn.info/855/044/000/ob7t9j3ebl9adHWNppO-o.jpg",
    name: "สปาเกตตี้",
    price: 500,
    category: "inter",
    group: 'fried'
  },
  {
    image: "https://f.ptcdn.info/855/044/000/ob7t9j3ebl9adHWNppO-o.jpg",
    name: "สปาเกตตี้",
    price: 500,
    category: "inter",
    group: 'fried'
  },
  {
    image: "https://f.ptcdn.info/855/044/000/ob7t9j3ebl9adHWNppO-o.jpg",
    name: "สปาเกตตี้",
    price: 500,
    category: "inter",
    group: 'fried'
  },
  {
    image: "https://f.ptcdn.info/855/044/000/ob7t9j3ebl9adHWNppO-o.jpg",
    name: "สปาเกตตี้",
    price: 500,
    category: "inter",
    group: 'fried'
  }
];

const list = async () => {
  try {
    // const res = await axios.get(
    //   "http://www.mocky.io/v2/5c837daa300000ca0b6b0c74"
    // );
    // return res.data;
    return mock
  } catch (error) {
    throw error;
  }
};

export default {
  list
};
