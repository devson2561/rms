import React, { Component } from "react";

import HomePage from "../../pages/home/Home.page";
import ProductService from "../../services/product.service";

export default class HomePageContainer extends Component {
  state = {
    products: []
  };

  componentDidMount = () => {
    this.fetchProducts("thai");
  };

  fetchProducts = async (category, group) => {
    let products = await ProductService.list();
    if (category && category !== null) {
      products = await products.filter(p => p.category === category);
    }

    if (group && group !== null && group !== "all") {
      products = await products.filter(p => p.group === group);
    }

    this.setState({
      products
    });
  };

  render() {
    const { products } = this.state;
    return <HomePage products={products} fetchProducts={this.fetchProducts} />;
  }
}
