import React from "react";
import HomePage from "./Home.page";
import { shallow, render, mount } from "enzyme";

describe("Home Page", () => {
  it("should render Home page without crashing", () => {
    shallow(<HomePage />);
  });

  it("should render CategoryTab component inside HomePage", async () => {
    const wrapper = render(<HomePage />);
    expect(wrapper.find("#category-tab")).toHaveLength(1);
  });
});
