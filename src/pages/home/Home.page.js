import React, { Component } from "react";

// components
import { CategoryTab, Products } from "../../components";

const options = [
  { value: 'grill', label: 'ย่าง' },
  { value: 'fried', label: 'ผัด/ทอด' },
  { value: 'all', label: 'ทั้งหมด' }
]
export default class HomePage extends Component {
  state = {
    category: "thai",
    group: 'all'
  };

  onChangeCategory = category => {
    const { group } = this.state
    this.props.fetchProducts(category, group);
    this.setState({ category });
  };

  onChangeGroup = group => {
    const { category } = this.state
    this.props.fetchProducts(category, group);
    this.setState({ group });
  };

  test = () => {
    window.location = 'https://pantip.com'
  }

  render() {
    const { category, group } = this.state;
    const { products } = this.props;
    return (
      <section className="section" id="home-page">
        <CategoryTab onChange={this.onChangeCategory} currentTab={category} />
        <br />
        <div className="columns is-tablet">
          <div className="column is-2">
            <ul className="button-vertical-group">

            {
              options.map((option, index) => {
                return (
                  <li>
                  <button id={`${option.value}-group-button`} onClick={() => this.onChangeGroup(option.value)} className={`button ${group === option.value ? 'is-primary' : 'is-default'} is-fullwidth`}>{option.label}</button>{" "}
                </li>
                )
              })
            }
            </ul>
          </div>
          <div className="column is-10">
            <Products products={products} category={category} group={group} />
          </div>
        </div>
      </section>
    );
  }
}
