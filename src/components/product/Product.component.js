import React from "react";

const Products = props => {
  const { image, name, price } = props.data;
  return (
    <div className="column is-3 product-component">
      <div className="card">
        <div className="card-image">
          <figure className="image is-4by3">
            <img src={image} alt={name} />
          </figure>
        </div>
        <div className="card-content">
          <h3 className="product-name">{name}</h3>
          <h3 className="product-name">ราคา {price} บาท</h3>
        </div>
      </div>
    </div>
  );
};

export default Products;
