import React from "react";
import CategoryTab from "./CategoryTab.component";
import { shallow, render } from "enzyme";

describe("Category tab", () => {
  it("should render CategoryTab without crashing", () => {
    shallow(<CategoryTab />);
  });

  it("should render 3 buttons of category tab", async () => {
    const wrapper = render(<CategoryTab />);
    expect(wrapper.find(".category-tab-item")).toHaveLength(3);
  });
});
