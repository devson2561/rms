import React from "react";
import PropTypes from "prop-types";

const options = [
  { value: "thai", label: "ไทย" },
  { value: "chinese", label: "จีน" },
  { value: "inter", label: "นานาชาติ" }
];

const CategoryTab = props => {
  const { currentTab, onChange } = props;
  return (
    <div id="category-tab">
      <div className="tabs is-large is-centered">
        <ul>
          {options.map((option, index) => (
            <li
              key={index}
              className={`category-tab-item ${
                currentTab === option.value ? "is-active" : ""
              }`}
              id={`${option.value}-category-button`}
            >
              <a href={false} onClick={() => onChange(option.value)}>{option.label}</a>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

CategoryTab.defaultProps = {
  currentTab: "thai"
};

CategoryTab.propTypes = {
  onChange: PropTypes.func,
  currentTab: PropTypes.string
};

export default CategoryTab;
