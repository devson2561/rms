import React from "react";
import Products from "./Products.component";
import { shallow, render } from "enzyme";

import ProductService from "../../services/product.service";

describe("Products component", () => {
  it("should render Products Component without crashing", () => {
    shallow(<Products />);
  });

  it("should render Product component inside Products", async () => {
    const products = await ProductService.list();
    const wrapper = render(<Products products={products} />);
    expect(wrapper.find(".product-component")).toHaveLength(products.length);
  });
});
