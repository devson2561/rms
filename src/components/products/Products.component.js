import React from "react";

import Product from "../product/Product.component";

const renderProducts = products => {
  return products.map((product, index) => {
    return <Product data={product} key={index} />;
  });
};

const Products = props => {
  const { products, group, category } = props;
  return (
    <div
      id="products"
      className={`columns is-tablet is-multiline ${category}-category-tab ${group}-group-tab`}
    >
      {renderProducts(products)}
    </div>
  );
};

Products.defaultProps = {
  products: []
};

export default Products;
