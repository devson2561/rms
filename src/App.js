import React, { Component } from "react";
import {
  HashRouter as Router,
  Route,
  Switch
} from "react-router-dom";
import "bulma/css/bulma.css";

import "./App.css";

// pages
import HomePage from "./containers/pages/HomePage.container";

const wait = time => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve();
    }, time);
  });
};

class App extends Component {
  state = {
    isLoading: true
  };

  componentDidMount() {
    wait(2000).then(() => {
      this.setState({ isLoading: false });
    });
  }

  componentDidUpdate() {
    if (!this.state.isLoading) {
      this.props.updateStatus();
    }
  }

  render() {
    if (this.state.isLoading) {
      return <h1>Loading</h1>;
    }

    return (
      <div id="app">
        <Router>
          <Switch>
            <Route path="/" component={HomePage} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
