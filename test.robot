*** Variables ***

${HOST}                 localhost
${PORT}                 3000
${SERVER}               http://${HOST}:${PORT}
# ${BROWSER}              headlesschrome
${BROWSER}              gc
${MESSAGE}              I'm working
${DELAYTIME}            0.5s
*** Settings ***

Documentation   ReactLibrary Acceptance Tests
Library         SeleniumLibrary  timeout=5  implicit_wait=0
Library         ReactLibrary
Library         DebugLibrary
Library         OperatingSystem
Library         WebpackLibrary

Suite Setup     Start React and open browser
Suite Teardown  Stop React and close browser

*** Keywords ***

Start React and Open Browser
  Set environment variable  BROWSER  none
  Log to console  starting server
  Start Webpack  npm start
  Log to console  server startd
  Open Browser  ${SERVER}  ${BROWSER}
  Set Window Size  1280  1024
  

Stop React and Close Browser
  # stop react

  Stop Webpack
  Close Browser



*** Test Cases ***

Scenario: Wait for react keyword can be called
  Wait for react

เข้าหน้ารายการอาหารสำเร็จ
  Go to  ${SERVER}
  Wait for react
  Wait Until Page Contains Element  id:home-page
  Sleep    ${DELAYTIME}


โหลดรายการอาหารสำเร็จ หลังจากเข้าหน้ารายการอาหาร
  Wait Until Page Contains Element  class:product-component
  Sleep    ${DELAYTIME}

กดปุ่ม อาหารไทย แล้วแสดงหมวดหมู่กับเมนูถูกต้อง
  Click Element  //*[@id="thai-category-button"]
  Wait Until Page Contains Element  class:thai-category-tab
  Sleep    ${DELAYTIME}

กดปุ่ม อาหารจีน แล้วแสดงหมวดหมู่กับเมนูถูกต้อง
  Click Element  //*[@id="chinese-category-button"]
  Wait Until Page Contains Element  class:chinese-category-tab
  Sleep    ${DELAYTIME}

กดปุ่ม อาหารนานาชาติ แล้วแสดงหมวดหมู่กับเมนูถูกต้อง
  Click Element  //*[@id="inter-category-button"]
  Wait Until Page Contains Element  class:inter-category-tab
  Sleep    ${DELAYTIME}

กดปุ่ม หมวดหมู่ ผัด/ทอด แล้วแสดงเมนูถูกต้อง
  Click Element  //*[@id="grill-group-button"]
  Wait Until Page Contains Element  class:grill-group-tab
  Sleep    ${DELAYTIME}

กดปุ่ม หมวดหมู่ผัด แล้วแสดงเมนูถูกต้อง
  Click Element  //*[@id="fried-group-button"]
  Wait Until Page Contains Element  class:fried-group-tab
  Sleep    ${DELAYTIME}

กดปุ่ม หมวดหมู่ทั้งหมด แล้วแสดงเมนูถูกต้อง
  Click Element  //*[@id="all-group-button"]
  Wait Until Page Contains Element  class:all-group-tab
  Sleep    ${DELAYTIME}
